require 'byebug'
class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    @referee.pick_secret_word
    @guesser.register_secret_length(@referee.pick_secret_word)
    @board = @referee.pick_secret_word
  end

  def take_turn
    @guesser.guess
    @referee.check_guess(@guesser.guess)
    self.update_board
    @guesser.handle_response
  end

  def update_board
    @board -= @referee.check_guess.size
  end

end

class HumanPlayer
  def pick_secret_word
    @dictionary[0].length
  end
  def register_secret_length
  end
  def guess
  end
  def check_guess(letter)
    indices = []
    if @dictionary[0].include?(letter)
      @dictionary[0].each_char.with_index do |let, idx|
        indices << idx if let == letter
      end
    end
    indices
  end
  def handle_response
  end
end

class ComputerPlayer

  attr_reader :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = dictionary
  end

  def pick_secret_word
    @secret = @dictionary[0]
    @secret.length
    # all_words = File.readlines('./lib/dictionary.txt')
    # random_num = rand(all_words.length)
    # all_words[random_num].delete(".,'\n/'").length
  end

  def register_secret_length(length)
    @candidate_words = self.candidate_words.reject { |w| w.size != length }
  end

  def guess(letter)
    # abc = ('a'..'z').to_a
    # abc.sample

    max_cnt, max2_cnt = 0, 0
    max_ltr, max2_ltr = '', ''
    flat_join = @candidate_words.flatten.join('')
    flat_join.each_char do |let|
      # debugger
      if flat_join.count(let) > max_cnt
        max_cnt = flat_join.count(let)
        max_ltr = let
      elsif flat_join.count(let) > max2_cnt && flat_join.count(let) < max_cnt
        max2_cnt = flat_join.count(let)
        max2_ltr = let
      end
    end

    return max2_ltr if letter.compact.size > 0
    return max_ltr if letter.compact.size == 0

  end


  def check_guess(letter)
    indices = []
    if @dictionary[0].include?(letter)
      @dictionary[0].each_char.with_index do |let, idx|
        indices << idx if let == letter
      end
    end
    indices
  end

  def handle_response(letter, idxs)
    @candidate_words.each do |word|
      word.each_char.with_index do |let, idx|
        @candidate_words.delete(word) if word.count(let) > idx.size
        @candidate_words.delete(word) if letter == let && !idxs.include?(idx)
      end
    end

    @candidate_words.each do |word|
      idxs.each do |id|
        @candidate_words.delete(word) if word[id] != letter
      end
    end
  end

end
